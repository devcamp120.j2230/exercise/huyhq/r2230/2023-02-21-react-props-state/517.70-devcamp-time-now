import TimeNow from "./component/TimeNow";

function App() {
  return (
    <div>
      <TimeNow/>
    </div>
  );
}

export default App;
