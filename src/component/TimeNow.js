import { Component } from "react";

class TimeNow extends Component{
    constructor(props){
        super(props);
        var date = new Date();
        this.state = {
            checkColor: "",
            hour: date.getHours(),
            minute: date.getMinutes(),
            second: date.getSeconds(),
        };

    }
    onClickChangeColor = ()=>{
        this.setState({
            checkColor: this.state.second %2 === 0 ? "red" : "blue"
        })
    }

    render(){
        return(
            <>
            <h1 style={{color: this.state.checkColor}}>Time now: {this.state.hour}h{this.state.minute}p{this.state.second}</h1>
            <button onClick={this.onClickChangeColor}>Change Color</button>
            </>
        )
    }
}

export default TimeNow